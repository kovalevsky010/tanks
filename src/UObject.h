#pragma once

#include <string>
#include <any>
#include <map>

class IUObject
{
public:
    virtual std::any& operator[](const std::string& key) = 0;
    virtual ~IUObject() = default;
};

class UObject : public IUObject
{
private:
    std::map<std::string, std::any> properties;

public:

    UObject();
    std::any& operator[](const std::string& key);
    ~UObject();
};

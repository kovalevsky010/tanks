#include "commands.h"

void BurnFuelCommand::Execute()
{
    int current_level = m_fuelable.getFuelLevel();
    m_fuelable.setFuelLevel(current_level - m_to_burn);
}

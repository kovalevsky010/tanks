#pragma once

#include "interfaces.h"

class Command
{
public:
    virtual void Execute() = 0;
    virtual ~Command() = default;
};

class BurnFuelCommand : public Command
{
private:
    Fuelable<int>& m_fuelable;
    int m_to_burn;

public:
    BurnFuelCommand(Fuelable<int>& fuelable, int to_burn) : m_fuelable(fuelable), m_to_burn(to_burn)
    {
    }

    void Execute();

    ~BurnFuelCommand() 
    {    
    }
};

template <typename Vector>
class MoveCommand : public Command 
{
private:
    Movable<Vector>& m_obj;
    Vector position;
public:

    MoveCommand(Movable<Vector>& obj, Vector position) : m_obj(obj), position(position)
    {
    }

    void Execute()
    {
        m_obj.setPosition(position);
    };
    
    ~MoveCommand() = default;
};
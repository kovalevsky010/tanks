#pragma once

#include <vector>

#include "commands.h"

class MacroCommand : public Command 
{
private:   
    std::vector<Command*> commands;
public:

    MacroCommand(const std::vector<Command*>& commands) : commands(commands)
    {
    }

    void Execute();
};

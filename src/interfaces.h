#pragma once

#include "UObject.h"
#include "resolve.h"

template <typename T>
class Movable
{
public:
    virtual T getPosition() const = 0;
    virtual void setPosition(T const &newValue) = 0;
    virtual T getVelocity() const = 0;
    virtual ~Movable() = default;
};

template <typename T>
class Fuelable
{
public:
    virtual T getFuelLevel() const = 0;
    virtual void setFuelLevel(const T& newValue) = 0;
    virtual ~Fuelable() = default;
};

template <typename T>
class FuelableImpl : public Fuelable<T>
{
private:
    UObject& m_obj;

public:
    FuelableImpl(UObject& obj) : m_obj(obj)
    {
    }

    T getFuelLevel() const
    {
        return resolve<T>("fuel_level", m_obj);
    };
    
    void setFuelLevel(const T& newValue)
    {
        resolve<T>("fuel_level", m_obj, newValue);
    };
    
    ~FuelableImpl() = default;
};

template <typename T>
class MovableImpl : public Movable<T>
{
private:
    UObject& m_obj;

public:
    MovableImpl(UObject& obj) : m_obj(obj)
    {   
    }

    T getPosition() const
    {
        return resolve<T>("position", m_obj);
    };

    void setPosition(const T& newValue)
    {
        resolve<T>("position", m_obj, newValue);
    };

    T getVelocity() const
    {
        return resolve<T>("velocity", m_obj);
    };

    ~MovableImpl() = default;
};
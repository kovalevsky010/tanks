#include "macro_command.h"

void MacroCommand::Execute()
{
    for (auto& command : commands) {
        command->Execute();
    }
}

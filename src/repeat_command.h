#pragma once

#include <queue>

#include "commands.h"
#include "macro_command.h"

class RepeatCommand : public Command {
 private:
    std::queue<Command*>& queue;
    MacroCommand* macro;
    int quantity_repeat;
 public:
    
    RepeatCommand(std::queue<Command*>& queue, MacroCommand* macro, int quantity_repeat) : macro(macro),
        queue(queue), quantity_repeat(quantity_repeat) {};

    void Execute() {
        queue.push(macro);
        --quantity_repeat;
        if (quantity_repeat) {
            RepeatCommand repeat(queue, macro, quantity_repeat);
            queue.push(&repeat);
        }
    }
};

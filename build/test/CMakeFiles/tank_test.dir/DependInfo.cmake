# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/Projects/C++/Projects/tank/tanks/test/UObject-test.cpp" "D:/Projects/C++/Projects/tank/tanks/build/test/CMakeFiles/tank_test.dir/UObject-test.cpp.obj"
  "D:/Projects/C++/Projects/tank/tanks/test/commands-test.cpp" "D:/Projects/C++/Projects/tank/tanks/build/test/CMakeFiles/tank_test.dir/commands-test.cpp.obj"
  "D:/Projects/C++/Projects/tank/tanks/test/interfaces-test.cpp" "D:/Projects/C++/Projects/tank/tanks/build/test/CMakeFiles/tank_test.dir/interfaces-test.cpp.obj"
  "D:/Projects/C++/Projects/tank/tanks/test/macro-command-test.cpp" "D:/Projects/C++/Projects/tank/tanks/build/test/CMakeFiles/tank_test.dir/macro-command-test.cpp.obj"
  "D:/Projects/C++/Projects/tank/tanks/test/main.cpp" "D:/Projects/C++/Projects/tank/tanks/build/test/CMakeFiles/tank_test.dir/main.cpp.obj"
  "D:/Projects/C++/Projects/tank/tanks/test/repeat-command-test.cpp" "D:/Projects/C++/Projects/tank/tanks/build/test/CMakeFiles/tank_test.dir/repeat-command-test.cpp.obj"
  "D:/Projects/C++/Projects/tank/tanks/test/resolve-test.cpp" "D:/Projects/C++/Projects/tank/tanks/build/test/CMakeFiles/tank_test.dir/resolve-test.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../src"
  "_deps/googletest-src/googletest/include"
  "_deps/googletest-src/googletest"
  "_deps/googletest-src/googlemock/include"
  "_deps/googletest-src/googlemock"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "D:/Projects/C++/Projects/tank/tanks/build/src/CMakeFiles/tank_lib.dir/DependInfo.cmake"
  "D:/Projects/C++/Projects/tank/tanks/build/_deps/googletest-build/googletest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "D:/Projects/C++/Projects/tank/tanks/build/_deps/googletest-build/googlemock/CMakeFiles/gmock.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")

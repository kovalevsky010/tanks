#include <gtest/gtest.h>

#include "UObject.h"

TEST(UObject, normal)
{
    UObject a;

    int *number1_ptr = new int(100);
    int *number2_ptr = new int(200);

    a["number1_ptr"] = number1_ptr;
    a["number2_ptr"] = number2_ptr;

    ASSERT_EQ(std::any_cast<int *>(a["number1_ptr"]), number1_ptr);
    ASSERT_EQ(std::any_cast<int *>(a["number2_ptr"]), number2_ptr);

    *number2_ptr += 2;

    ASSERT_EQ(*std::any_cast<int *>(a["number2_ptr"]), 202);
}
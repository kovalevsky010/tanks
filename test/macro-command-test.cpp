#include <gtest/gtest.h>
#include <vector>
#include <iostream>

#include "UObject.h"
#include "interfaces.h"
#include "commands.h"
#include "resolve.h"
#include "macro_command.h"

TEST(macro_commnad, normal) 
{
    UObject* tank = new UObject();
    std::vector<int> start_position = {0, 0};
    std::vector<int> position = {1, 1};

    resolve("position", *tank, start_position);
    resolve("fuel_level", *tank, 100);

    FuelableImpl<int>* fuelable = new FuelableImpl<int>(*tank);
    BurnFuelCommand* burn_fuel = new BurnFuelCommand(*fuelable, 10);

    MovableImpl<std::vector<int>>* movable = new MovableImpl<std::vector<int>>(*tank);
    MoveCommand<std::vector<int>>* move = new MoveCommand<std::vector<int>>(*movable, position);

    std::vector<Command*> commands = {move, burn_fuel};
    MacroCommand* macro = new MacroCommand(commands);

    macro->Execute();

    ASSERT_EQ(resolve<int>("fuel_level", *tank), 90);

    std::vector<int> current_position = resolve<std::vector<int>>("position", *tank);

    for (int& number : current_position) {
        ASSERT_EQ(number, 1);
    }
};

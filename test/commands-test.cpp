#include <gtest/gtest.h>

#include "UObject.h"
#include "interfaces.h"
#include "commands.h"
#include "resolve.h"

#include <vector>
#include <iostream>

TEST(BurnFuelCommand, normal)
{
    UObject *tank = new UObject();
    FuelableImpl<int> *fuelable = new FuelableImpl<int>(*tank);
    resolve("fuel_level", *tank, 100);

    BurnFuelCommand *bf_command = new BurnFuelCommand(*fuelable, 10);

    std::cout << resolve<int>("fuel_level", *tank) << std::endl;

    for (int i = 0; i < 5; ++i) {
        bf_command->Execute();
        std::cout << resolve<int>("fuel_level", *tank) << std::endl;
    } 
}

TEST(MoveCommand, normal)
{
    UObject *tank = new UObject();
    MovableImpl<std::vector<int>>* moveable = new MovableImpl<std::vector<int>>(*tank);
    std::vector<int> position = {0, 0};
    resolve("position", *tank, position);

    std::vector<int> new_position = {1, 1};
    MoveCommand<std::vector<int>>* move_command = new MoveCommand<std::vector<int>>(*moveable, new_position);

    std::vector<int> current_position = resolve<std::vector<int>>("position", *tank);

    for (int& number : current_position) {
        ASSERT_EQ(number, 0);
    }

    move_command->Execute();
    current_position = resolve<std::vector<int>>("position", *tank);

    for (int& number : current_position) {
        ASSERT_EQ(number, 1);
    }
}

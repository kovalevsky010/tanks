#include <gtest/gtest.h>

#include "UObject.h"
#include "resolve.h"

TEST(resolve, normal)
{
    UObject a;
    UObject b;

    int *number1_ptr = new int(100);
    int *number2_ptr = new int(200);

    resolve("number1_ptr", a, number1_ptr);
    resolve("number2_ptr", a, number2_ptr);
    resolve("number1_ptr", b, number1_ptr);

    ASSERT_EQ(resolve<int *>("number1_ptr", a), number1_ptr);
    ASSERT_EQ(resolve<int *>("number2_ptr", a), number2_ptr);
    ASSERT_EQ(resolve<int *>("number1_ptr", b), number1_ptr);

    *number2_ptr += 2;

    ASSERT_EQ(*std::any_cast<int *>(a["number2_ptr"]), 202);
}
